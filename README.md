# gccPracticeTasks

### Task hello world program.

**g++ hello.cpp -o hello**

### Task calculator

**g++ main.cpp calculator.cpp -o calculator**
**./calculator**

### Task make own library

Used the following commands for making my library.

 **g++ -c calculator.cpp -o lib_calculator.o**

 **ar rcs lib_calculator.a lib_calculator.o**

 **g++ -c main.cpp -o calculator.o**

 **g++ -o calculator calculator.o -L. lib_calculator.a**

1.calculator.cpp contains functions for the calculator class.Compiled the calculator.cpp file.

2.Create static library. This step is to bundle multiple object files in one static library. The output of this step is static library.

3.Compiled main.cpp file.

4.linked the main.cpp object file with the library.
